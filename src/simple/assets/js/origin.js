/**
 * Clase que permite personalizar la pantalla de login en base al origen de la peticion
 * 
 */


var SimbiusOrigins = function (org) {
    this.origins = {
        "alicorp-bpm-qa.simbius.cloud": {
            "loginForm": {
                "appName": "BIENVENIDO A LA<p class=\"app-name\">MESA DE SERVICIO CSC</p>INICIE LA SESION",
                "showRecoverPwd": true,
                "showUserReg": true
            }
        },
        "alicorp-htr-qa.simbius.cloud": {
            "loginForm":{
                "appName": "BIENVENIDO A LA<p class=\"app-name\">MESA DE SERVICIOS GSC</p>INICIE LA SESION",
                "showRecoverPwd": false,
                "showUserReg": false
            }
        },
        "alicorpfast2.simbius.cloud": {
            "loginForm":{
                "appName": "BIENVENIDO A <p>FAST 2.0</p>INICIE LA SESION",
                "showRecoverPwd": false,
                "showUserReg": false
            }
        },
        "localhost": { //Este se agrega a fines de probar en entorno local
            "loginForm":{
                "appName": "BIENVENIDO A LA<p class=\"app-name\">MESA DE SERVICIO CSC</p>INICIE LA SESION",
                "showRecoverPwd": true,
                "showUserReg": true
            }
        }
    }

    this.origin = org;
}

$.extend (SimbiusOrigins.prototype, {

    /**
     * Devuelve los datos de acuerdo al origen de la peticion
     * 
     * @returns Object
     */
    getOriginData: function () {
        var retVal = null,
            self = this;

        Object.keys(self.origins).forEach(function (o) {
            if (o == self.origin) {
                retVal = self.origins[o];
            }
        });
        
        return retVal;
    },

    /**
     * Devuelve el mensaje de bienvenida asociado al origen de la peticion
     * 
     * @returns String
     */
    getWelcomeMessage: function () {
        var retVal = "BIENVENIDO <p>INICIE LA SESIÓN</p> ",
            oData = this.getOriginData();

        if (oData) {
            retVal = oData.loginForm.appName;
        }

        return retVal;
    },

    /**
     * Devuelve la configuracion de visibilidad del link Recuperar Password
     * 
     * @returns Boolean
     */
    showRecoverPwd: function () {
        var retVal = true,
            oData = this.getOriginData();

        if (oData) {
            retVal = oData.loginForm.showRecoverPwd;
        }

        return retVal;
    },

    /**
     * Devuelve la configuracion de visibilidad del link Registrar Usuario
     * 
     * @returns Boolean
     */
     showUserReg: function () {
        var retVal = true,
            oData = this.getOriginData();

        if (oData) {
            retVal = oData.loginForm.showUserReg;
        }

        return retVal;
    }
})
