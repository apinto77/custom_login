let card = document.querySelector('#CardElem');
let loader = document.querySelector('#loader');
const loginFormElm = document.querySelector('#LoginFormElm');

function getUrlParam(param) {
    const urlParams = new URLSearchParams(window.location.search);

    return urlParams.get(param);
}

//Login success handler
loginFormElm.addEventListener('loginSuccess', function (eventData) {
    console.log(eventData)


    const redirect = getUrlParam('redirect');
    const returnUrl = getUrlParam('returnUrl');

    let sURL = 'agileapps/dashboard';
    if(redirect) {            
        console.log('Redireccionando a: ', redirect);
        if(redirect !== '') {
            sURL = redirect;
        } else {
            console.log('No se encontro redireccionamiento... Se redirigira a /agileapps');            
        }
    } else {
        if (returnUrl) { //Se debe redireccionar al form que muestra el registro del objeto seleccionado

            console.log("searchstring: ", returnUrl);
            let sTemp = returnUrl.substring(returnUrl.lastIndexOf("=") + 1).split("/");
            let object = sTemp[0];
            let record = sTemp[1];

            sURL = "agileapps/records/detail/" + object + "/" + record
        }
    }
    sURL = window.location.origin + '/' + sURL;
    window.location.href = sURL;

})

//Login Error handler
loginFormElm.addEventListener('loginError', function (eventData) {
    const errorMessage = eventData['detail']['__exception_msg__'];
})

/**
 * Personaliza el mensaje de bienvenida segun de donde venga la peticion
 * Tambien oculta/muestra los links de Recuperar Pwd y Registrar Usuario dependiendo del origen de la peticion
 * 
 */
function customizeLogin () {
    const welcomeMsg = document.querySelector('#welcomeMsg');
    const objRecoverPwd = document.querySelector('#objRecoverPwd');
    const objRegisterUsr = document.querySelector('#objRegisterUsr');
    const simbiusOrigins = new SimbiusOrigins(window.location.hostname);
    
    
    welcomeMsg.innerHTML = simbiusOrigins.getWelcomeMessage();
    
    if (!simbiusOrigins.showRecoverPwd()) {
        objRecoverPwd.style= "display: none";
    }
    
    if (!simbiusOrigins.showUserReg()) {
        objRegisterUsr.style= "display: none";
    }
    
}


// ************************* Comienza ejecucion

setTimeout(() => { card.hidden =  false; loader.hidden = true }, 3000);

customizeLogin();